![MacDown logo](https://www.moosefactory.eu/resources/MooseFactoryRoundLogo.png)

**Utility static library for iOS or MacOS**


This is a compilation of some of commonly used codes over the years.  

Tristan Leblanc - MooseFactory Software  
<https://www.moosefactory.eu>

GiHub : 
<https://github.com/moosefactory>

***

## Table of Content

***

**MFExtras**

* NSArray+MFExtras
    * arrayByRemovingLastObject
    * arrayByRemovingFirstObject
    * alphabeticallySortedArray
* NSData+MFExtras
    * dataWithBytesString:
    * bytesString
    * randomDataOfLength:
* NSDate+MFExtras
    * dateByClearingTime
    * dateBySettingHour:
    * isSameDayAsDate:
    * isSameMonthAsDate:
    * oneMonthLater
    * oneMonthEarlier
    * oneDayLater
    * oneDayEarlier
    * secondsSinceStartOfDay
    * components
* NSDictionary+MFExtras
    * dictionaryWithName:
    * dictionaryWithName:
    * localizedDictionaryWithName:
    * localizedDictionaryWithName:
    * alphabeticallySortedKeys
* NSFileManager+MFExtras
    * isDirectoryEmpty:
    * NSString+MFExtras
* UUIDString
    * md5
    * sha1
    * isValidEmail:
    * isValidName
    * urlEncode
    * urlDecode

***

**Date and Formatters**

* MFChronoTimeFormatter
* MFDateUtils
* MFFormatters
* MFLatLongFormatter

***

**Types**

* MFTypes
* MFDateRange
* MFPeriod

***

**System**

* MFSystem

***

Standard C functions :

**Maths**

* MFFastTrigo <a href="./docs/HeaderDocs/MFFastTrigo_h/index.html">Documentation</a>
* MFMathUtilities <a href="./docs/HeaderDocs/MFMathUtilities_h/index.html">Documentation</a>

**Graphics**

* MFCombinedColor <a href="./docs/HeaderDocs/MFCombinedColor_h/index.html">Documentation</a>

***

##Author

Tristan Leblanc <tristan@moosefactory.eu>

Twitter     :	<https://www.twitter.com/tristanleblanc>  
Google+     :	<https://plus.google.com/+TristanLeblanc>  

***

##License

MFFoundation is available under the MIT license. See the LICENSE file for more info.

***
